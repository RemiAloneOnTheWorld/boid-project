using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SimpleBoid : MonoBehaviour
{
    BoidSettings settings;

    //state of the Boid
    [HideInInspector]
    public Vector3 position;
    
    [HideInInspector]
    public Vector3 forward;

    private Vector3 velocity;

    //variables that need to be updated
    private Vector3 acceleration;
    
    [HideInInspector]
    public Vector3 avgFlockHeading;
    
    [HideInInspector]
    public Vector3 avgAvoidanceHeading;

    [HideInInspector]
    public Vector3 centerOffFlock;

    [HideInInspector]
    public int numPerceivedFlockMates;

    //cached data
    private Material material;
    private Transform cachedTransform;
    private Transform target;

    private void Awake() {
        material = transform.GetComponentInChildren<MeshRenderer>().material;
        cachedTransform = transform;
    }

    public void Initialize(BoidSettings settings, Transform target) {
        this.target = target;
        this.settings = settings;

        position = cachedTransform.position;
        forward = cachedTransform.forward;

        float startSpeed = (settings.minSpeed + settings.maxSpeed) / 2;
        velocity = transform.forward * startSpeed;
    }

    public void UpdateBoid() {
        Vector3 acceleration = Vector3.zero;

        if(target != null) {
            Vector3 offSetToTarget = (target.position - position);
            acceleration = SteerTowards(offSetToTarget) * settings.targetWeight;
        }

        if(numPerceivedFlockMates != 0) {
            centerOffFlock /= numPerceivedFlockMates;

            Vector3 offSetToFlockmatesCenter = (centerOffFlock - position);

            var allignmentForce = SteerTowards(avgFlockHeading) * settings.alignWeight;
            var cohesionForce = SteerTowards(offSetToFlockmatesCenter) * settings.cohesionWeight;
            var seperationForce = SteerTowards(avgAvoidanceHeading) * settings.seperationWeight;

            acceleration += allignmentForce;
            acceleration += cohesionForce;
            acceleration += seperationForce;
        }

        if(IsHeadingForCollision()) {
            Vector3 collisionAvoidDirection = ObstacleRays();
            Vector3 collisionAvoidForce = SteerTowards(collisionAvoidDirection) * settings.avoidanceCollisionWeight;
            acceleration += collisionAvoidForce;
        }

        velocity += acceleration * Time.deltaTime;
        float speed = velocity.magnitude;
        Vector3 direction = velocity / speed;
        speed = Mathf.Clamp(speed, settings.minSpeed, settings.maxSpeed);
        velocity = direction * speed;

        cachedTransform.position += velocity * Time.deltaTime;
        cachedTransform.forward = direction;
        position = cachedTransform.position;
        forward = direction;
    }

    private bool IsHeadingForCollision() {
        RaycastHit hit;
        if(Physics.SphereCast(position, settings.boundsRadius, forward, out hit, settings.collisionAvoidDistance, settings.obstacleMask)) {
            return true;
        }
        return false;
    }

    private Vector3 ObstacleRays() {
        Vector3[] rayDirections = BoidHelper.directions;
        
        for (int i = 0; i < rayDirections.Length; i++)
        {
            Vector3 direction = cachedTransform.TransformDirection(rayDirections[i]);
            Ray ray = new Ray(position, direction);
            if(!Physics.SphereCast(ray,settings.boundsRadius, settings.collisionAvoidDistance, settings.obstacleMask)) {
                return direction;
            }
        }
        return forward;
    }

    private Vector3 SteerTowards(Vector3 vector) {
        Vector3 newVelocity = vector.normalized* settings.maxSpeed - velocity;
        return Vector3.ClampMagnitude(newVelocity, settings.maxSteerForce); 
    }
}
